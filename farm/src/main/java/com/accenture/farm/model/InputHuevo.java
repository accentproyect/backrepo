package com.accenture.farm.model;

public class InputHuevo {
	
	private Long idGallina;
	private Huevo huevo;
	
	public Long getIdGallina() {
		return idGallina;
	}
	public void setIdGallina(Long idGallina) {
		this.idGallina = idGallina;
	}
	public Huevo getHuevo() {
		return huevo;
	}
	public void setHuevo(Huevo huevo) {
		this.huevo = huevo;
	}
	
	

}
