package com.accenture.farm.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "gallinas")
public class Gallina {

	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	private int huevostot=0;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "granja_id")
	private Granja granja;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "gallina")
	private List<Huevo> huevos;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getHuevostot() {
		return huevostot;
	}

	public void setHuevostot(int huevostot) {
		this.huevostot = huevostot;
	}

	public Granja getGranja() {
		return granja;
	}

	public void setGranja(Granja granja) {
		this.granja = granja;
	}

	public List<Huevo> getHuevos() {
		return huevos;
	}

	public void setHuevos(List<Huevo> huevos) {
		this.huevos = huevos;
	}
	
	
	
}
