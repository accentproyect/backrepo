package com.accenture.farm.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name= "granjas")
public class Granja {
	
	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "granja")
	private List<Gallina> gallinas;
	
	public List<Gallina> getGallinas() {
		return gallinas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	
}
