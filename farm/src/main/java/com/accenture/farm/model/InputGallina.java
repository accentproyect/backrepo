package com.accenture.farm.model;

public class InputGallina {
	
	private Long idGranja;
	private Gallina gallina;
	
	public Long getIdGranja() {
		return idGranja;
	}
	public void setIdGranja(Long idGranja) {
		this.idGranja = idGranja;
	}
	public Gallina getGallina() {
		return gallina;
	}
	public void setGallina(Gallina gallina) {
		this.gallina = gallina;
	}
	

}
