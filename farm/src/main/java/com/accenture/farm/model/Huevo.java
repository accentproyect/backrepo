package com.accenture.farm.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name= "huevos")
public class Huevo {
	
	@Id
	@GeneratedValue
	private Long id;
	private String color;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "gallina_id")
	private Gallina gallina;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Gallina getGallina() {
		return gallina;
	}

	public void setGallina(Gallina gallina) {
		this.gallina = gallina;
	}
	
	

}
