package com.accenture.farm.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.accenture.farm.model.Gallina;
import com.accenture.farm.model.Granja;
import com.accenture.farm.model.Huevo;
import com.accenture.farm.model.InputGallina;
import com.accenture.farm.model.InputHuevo;
import com.accenture.farm.repository.GallinaDao;
import com.accenture.farm.repository.GranjaDao;
import com.accenture.farm.repository.HuevoDao;


@RestController
@RequestMapping({"/granja"})
public class MainController {
	
	@Autowired
	private GallinaDao gallinadao;
	
	@Autowired
	private GranjaDao granjadao;
	
	@Autowired
	private HuevoDao huevodao;
	
	
	//doy de alta una granja, el id se genera solo
		@CrossOrigin(origins = "http://localhost:4200")
		@PostMapping
		public Granja createGranja(@RequestBody Granja granja) {
		
			return granjadao.save(granja);
	}
	
	//doy de alta una gallina, asignandole una granja desde el front (por id granja)
		@CrossOrigin(origins = "http://localhost:4200")
		@PostMapping(path= {"/gallinas"})
		public ResponseEntity<Object> createGallina(@RequestBody InputGallina gallina) {
			
			Granja granja1 = granjadao.findById(gallina.getIdGranja()).orElse(null);
			Gallina gallina1 = new Gallina();
			
			JSONObject obj = new JSONObject();
			
			if(granja1 != null) {
				
				gallina1.setGranja(granja1); 
				gallina1.setNombre(gallina.getGallina().getNombre());
				
				Gallina galli = gallinadao.save(gallina1);
				
				obj.put("error", 0);
				obj.put("id de la gallina", galli.getId());
				
				return ResponseEntity.ok().body(obj.toString());
				
			} else {
				
				obj.put("error", 1);
				obj.put("message", "NO SE ENCONTRO LA GRANJA ELEGIDA");
				
				return ResponseEntity.ok().body(obj.toString());
			}
		}
		
	//doy de alta un huevo, asignandole una gallina desde el front (por id gallina)
		@CrossOrigin(origins = "http://localhost:4200")
		@PostMapping(path= {"/huevos"})
		public ResponseEntity<Object> createHuevo(@RequestBody InputHuevo huevo){
			
			Gallina galli1 = gallinadao.findById(huevo.getIdGallina()).orElse(null);
			Huevo huevo1 = new Huevo();
			
			JSONObject obj = new JSONObject();
			
			if(galli1 != null) {
				
				huevo1.setGallina(galli1);
				huevo1.setColor(huevo.getHuevo().getColor());
				
				Huevo huevi = huevodao.save(huevo1);
				
				obj.put("error", 0);
				obj.put("id del huevo", huevi.getId());
				
				return ResponseEntity.ok().body(obj.toString());
				
			} else {
				
				obj.put("error", 1);
				obj.put("message", "NO SE ENCONTRO LA GALLINA ELEGIDA");
				
				return ResponseEntity.ok().body(obj.toString());
				
			}
			
		}
		
		//muestro todas las granjas
		@CrossOrigin(origins = "http://localhost:4200")
		@GetMapping
		public ResponseEntity<Object> listarGranjas() {
			
			List<Granja> granjasFound = granjadao.findAll();
			JSONArray json_array = new JSONArray();
			
			if(granjasFound.size()>0) {
				
				for (Granja gran : granjasFound) {
					
					JSONObject aux = new JSONObject();
					aux.put("id", gran.getId());
					aux.put("nombre", gran.getNombre());
					json_array.put(aux);
				}
				
				JSONObject obj = new JSONObject();
				obj.put("error", 0);
				obj.put("results", json_array);
				
				return ResponseEntity.ok().body(obj.toString());
				
			} else {
				
				JSONObject obj = new JSONObject();
				obj.put("error", 1);
				obj.put("message", "NO SE ENCONTRARON GRANJAS");
				
				return ResponseEntity.ok().body(obj.toString());
			}
			
			
			
		}
		
	//muestro todos los huevos que existen en una gallina
		@CrossOrigin(origins = "http://localhost:4200")
		@GetMapping(path = {"/gallinas/{id}"})
		public ResponseEntity<Object> listarGallinas(@PathVariable("id") Long id){
			
			Gallina galli1 = gallinadao.findById(id).orElse(null);
			
			if (galli1 != null) {
				
				List<Huevo> huevosFound = huevodao.findByGallina(galli1);
				
				JSONArray json_array = new JSONArray();
				
				if(huevosFound.size() > 0) {
					
					for(Huevo hue : huevosFound) {
						
						JSONObject aux = new JSONObject();
						
						aux.put("color", hue.getColor());
						
						json_array.put(aux);
					}
					
				} else {
					
					JSONObject objerror = new JSONObject();
					objerror.put("error", 1);
					objerror.put("results", "NO HAY HUEVOS");
					
					return ResponseEntity.ok().body(objerror.toString());
				}
				
				JSONObject obj = new JSONObject();
				obj.put("error", 0);
				obj.put("nombre", galli1.getNombre());
				obj.put("huevos", json_array);
				
				return ResponseEntity.ok().body(obj.toString());
				
			} else {
				
				JSONObject objerror = new JSONObject();
				objerror.put("error", 1);
				objerror.put("message", "NO SE ENCONTRO LA GALLINA");
				
				return ResponseEntity.ok().body(objerror.toString());
			}
		}
		
	
	//muestro todas las gallinas que existen en una granja (similar al anterior)
		@CrossOrigin(origins = "http://localhost:4200")
		@GetMapping(path = {"/granjas/{id}"})
		public ResponseEntity<Object> listarGranjas(@PathVariable("id") Long id){
			
			Granja granja1 = granjadao.findById(id).orElse(null);
			
			if (granja1 != null) {
				
				List<Gallina> gallinasFound = gallinadao.findByGranja(granja1);
				
				JSONArray json_array = new JSONArray();
				
				if(gallinasFound.size() > 0) {
					
					for(Gallina galli : gallinasFound) {
						
						JSONObject aux = new JSONObject();
						
						aux.put("nombre", galli.getNombre());
						aux.put("id", galli.getId());
						//si quiero puedo agregar huevostotales despues
						
						json_array.put(aux);	
					}
					
					
				} else {
					
					JSONObject objerror = new JSONObject();
					objerror.put("error", 1);
					objerror.put("results", "NO HAY GALLINAS");
					
					return ResponseEntity.ok().body(objerror.toString());
					
				}
				
				JSONObject obj = new JSONObject();
				obj.put("error", 0);
				obj.put("nombre", granja1.getNombre());
				obj.put("gallinas", json_array);
				
				return ResponseEntity.ok().body(obj.toString());
				
			} else {
				
				JSONObject objerror = new JSONObject();
				objerror.put("error", 1);
				objerror.put("message", "NO SE ENCONTRO LA GRANJA");
				
				return ResponseEntity.ok().body(objerror.toString());
				
			}	
		}
		
	//borro una gallina de la base de datos
		@CrossOrigin(origins = "http://localhost:4200")
		@DeleteMapping(path = {"/{id}"})
		public ResponseEntity<Object> asesinar(@PathVariable("id") long id) {
			
			gallinadao.deleteById(id);
			
			JSONObject objerror = new JSONObject();
			objerror.put("error", 0);
			objerror.put("message", "Gallina degollada");
			
			return ResponseEntity.ok().body(objerror.toString());

		}
		
		
		
}
