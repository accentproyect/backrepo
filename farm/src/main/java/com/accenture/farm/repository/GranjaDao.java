package com.accenture.farm.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.farm.model.Granja;

public interface GranjaDao extends JpaRepository<Granja, Long> {

}
