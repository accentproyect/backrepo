package com.accenture.farm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.farm.model.Gallina;
import com.accenture.farm.model.Huevo;

public interface HuevoDao extends JpaRepository<Huevo, Long> {

	public List<Huevo> findByGallina(Gallina gallina);

}
