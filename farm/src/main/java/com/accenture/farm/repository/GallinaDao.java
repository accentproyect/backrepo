package com.accenture.farm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.accenture.farm.model.Gallina;
import com.accenture.farm.model.Granja;

public interface GallinaDao extends JpaRepository<Gallina, Long> {
	
	public List<Gallina> findByGranja(Granja granja);

}
